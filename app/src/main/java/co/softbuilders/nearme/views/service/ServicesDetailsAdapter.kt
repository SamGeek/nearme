package co.softbuilders.nearme.views.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Service


class ServicesDetailsAdapter(
    private val providerList: List<Service>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<ServicesDetailsAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        /*var providerName: TextView
        var providerImage: ImageView*/

        init {
           /* providerName = view.findViewById<View>(R.id.providerName) as TextView
            providerImage = view.findViewById<View>(R.id.providerImage) as ImageView
            view.findViewById<View>(R.id.provider).setOnClickListener(this)*/
        }

        override fun onClick(view: View) {
//            (mActivity as MenuActivity).loadServiceInterface(providerList?.get(adapterPosition)?.serviceName)
            // Compte currentCompte = AccountActivity.Companion.getMCompteList().get(getAdapterPosition());
//            mActivity.startActivity(Intent(mActivity, RechargeDetailsActivity::class.java).putExtra("provider",Gson().toJson(providerList!!.get(adapterPosition))))

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.most_ordered_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        /*val (serviceImageId, serviceName) = providerList!![position]
        holder.providerName.text = serviceName
        holder.providerImage.setImageDrawable(mActivity.resources.getDrawable(serviceImageId))*/
    }

    override fun getItemCount(): Int {
        return providerList!!.size
    }

    companion object {
        private val TAG = "ServiceAdapter"
    }

}
