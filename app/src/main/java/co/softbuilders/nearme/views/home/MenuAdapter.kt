package co.softbuilders.nearme.views.home

import android.content.Context
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.MenuItemView


class MenuAdapter(
    private val menuList: List<MenuItemView>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<MenuAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        var menuName: TextView
        var menuImage: ImageView

        init {
            menuName = view.findViewById<View>(R.id.menuName) as TextView
            menuImage = view.findViewById<View>(R.id.menuImage) as ImageView
            view.findViewById<View>(R.id.container ).setOnClickListener(this)
        }

        override fun onClick(view: View) {

            (mActivity as MenuActivity).loadAppropriateMenuContent(menuList?.get(adapterPosition)?.menuName)
            // Compte currentCompte = AccountActivity.Companion.getMCompteList().get(getAdapterPosition());
            //mActivity.startActivity(Intent(mActivity, RechargeDetailsActivity::class.java).putExtra("",""))

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.menu_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val (resourceID, providerName) = menuList!![position]
        holder.menuName.text = providerName
        holder.menuImage.setImageDrawable(mActivity.resources.getDrawable(resourceID))
    }

    override fun getItemCount(): Int {
        return menuList!!.size
    }

    companion object {

        private val TAG = "MenuItemAdapter"
    }

}
