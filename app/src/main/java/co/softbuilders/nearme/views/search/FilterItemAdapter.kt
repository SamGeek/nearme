package co.softbuilders.nearme.views.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ToggleButton
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Service
import android.widget.CompoundButton
import android.widget.TextView


class FilterItemAdapter(
    private val providerList: List<Service>?,
    var mActivity: AppCompatActivity,
    private var onFilterItemChooseListener: SearchActivity.OnFilterItemChooseListener
) : RecyclerView.Adapter<FilterItemAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        var filterTypeTV: TextView
        /*var providerImage: ImageView*/
        var filterItemToggle : ToggleButton

        init {
            /*providerName = view.findViewById<View>(R.id.providerName) as TextView
            providerImage = view.findViewById<View>(R.id.providerImage) as ImageView*/
//            view.findViewById<View>(R.id.provider).setOnClickListener(this)\
            filterTypeTV = view.findViewById<View>(R.id.filterTypeTV) as TextView
            filterItemToggle = view.findViewById<View>(R.id.toggleButton) as ToggleButton

            filterItemToggle.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                val service = providerList!!.get(adapterPosition)
                if (isChecked) {
                    listSelectedFilters.add(service)
                    onFilterItemChooseListener.onEnable(listSelectedFilters, listSelectedFilters.indexOf(service))
                } else {
                    listSelectedFilters.remove(service)
                    onFilterItemChooseListener.onEnable(listSelectedFilters,listSelectedFilters.indexOf(service))
                }
            })

        }

        override fun onClick(view: View) {
//            (mActivity as MenuActivity).loadServiceInterface(providerList?.get(adapterPosition)?.serviceName)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.filter_criteria_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val service = providerList!![position]
        holder.filterTypeTV.text = service.serviceName
        /*holder.providerImage.setImageDrawable(mActivity.resources.getDrawable(serviceImageId))*/
    }

    override fun getItemCount(): Int {
        return providerList!!.size
    }

    companion object {

        private val TAG = "ServiceAdapter"
        var listSelectedFilters: MutableList<Service> = mutableListOf<Service>()
    }

}
