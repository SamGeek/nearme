package co.softbuilders.nearme.views.statistics

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.MostRated
import co.softbuilders.nearme.data.models.Trade
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_account_infos.headerText
import kotlinx.android.synthetic.main.activity_statistics.*
import kotlinx.android.synthetic.main.activity_statistics.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class StatisticsFragment : Fragment() {

    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var mAdapter: MostRatedAdapter?=null
    private lateinit var mDisposable: CompositeDisposable
    private lateinit var mostsRated : MutableList<MostRated>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
            val view = inflater.inflate(R.layout.activity_statistics, container, false)

//            if(intent.extras!=null){
//                tradeConnected = Gson().fromJson(intent.extras.getString("tradeConnected"),Trade::class.java)
//            }

        view.headerText.text = "Statistiques"

            mostsRated = mutableListOf()
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())
            mostsRated.add(MostRated())

            mAdapter = MostRatedAdapter(
                mostsRated,
                activity as AppCompatActivity
            )
            val mLayoutManager = GridLayoutManager(context,2)
            view.mostPopularRecycler.apply {
                layoutManager = mLayoutManager
                itemAnimator = DefaultItemAnimator()
                adapter = mAdapter
            }

            view.overflow.setOnClickListener {
                listener?.onOpenDrawer()
            }


        return view
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun onOpenDrawer()
    }

    companion object{
        lateinit var tradeConnected: Trade
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RestaurantFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            StatisticsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }





}
