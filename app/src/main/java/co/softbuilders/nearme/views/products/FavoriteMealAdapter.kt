package co.softbuilders.nearme.views.products

import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.data.models.TradeFull
import co.softbuilders.nearme.utils.Utils


class FavoriteMealAdapter(
    private var favoriteMeals: MutableList<TradeFull.FavoriteMeal>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<FavoriteMealAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view){
        var name: TextView
        var price: TextView
        var photo: ImageView

        init {
            name = view.findViewById<View>(R.id.name) as TextView
            price = view.findViewById<View>(R.id.price) as TextView
            photo = view.findViewById<View>(R.id.photo) as ImageView

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.favorite_meal_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val  favoriteMeal = favoriteMeals!![position]
        holder.name.text = favoriteMeal.name
        holder.price.text = "${favoriteMeal.price} Fcfa"
        Utils.glide(
            mActivity,
            favoriteMeal.photo.photoUrl,
            holder.photo,
            R.drawable.meal_image
            , false
        )
    }

    override fun getItemCount(): Int {
        return favoriteMeals!!.size
    }

    fun setItems(data: MutableList<TradeFull.FavoriteMeal>?): Unit {
        this.favoriteMeals = data
        notifyDataSetChanged()
    }

    fun addItem(item :TradeFull.FavoriteMeal?): Unit {
        this.favoriteMeals!!.add(item!!)
        notifyDataSetChanged()
    }

    companion object {

        private val TAG = "FavoriteMealAdapter"
    }

}
