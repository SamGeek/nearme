package co.softbuilders.nearme.views.joinus

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import co.softbuilders.nearme.R
import co.softbuilders.nearme.views.home.MenuActivity
import com.google.gson.Gson

class JoinUsActivity : AppCompatActivity(),
    FirstStepFragment.OnFragmentInteractionListener,
    SecondStepFragment.OnFragmentInteractionListener,
    ThirdStepFragment.OnFragmentInteractionListener{
    override fun onFirstStepDone(bundle: Bundle) {
        //load second step
        findNavController(R.id.nav_host_fragment).navigate(R.id.secondStepFragment3, bundle)
    }

    override fun onSecondStepDone(tradeSaved: Bundle) {
        //load third step
        findNavController(R.id.nav_host_fragment).navigate(R.id.thirdStepFragment2,tradeSaved)
    }

    override fun onThirdStepDone(tradeString: String) {
        //load menu
        startActivity(Intent(this@JoinUsActivity, MenuActivity::class.java).putExtra("tradeConnected", tradeString))
        this.finish()
    }

    override fun onFragmentInteraction(uri: Uri) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_us)
        initUI()
    }

    private fun initUI() {
        supportActionBar?.hide()
    }

    fun setActionBarTitle(title: String) {
        supportActionBar!!.title = title
    }

 /*   override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@JoinUsActivity,HomeActivity::class.java))
        this.finish()
    }
*/
}
