package co.softbuilders.nearme.views.statistics

import android.content.Context
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.MostRated


class MostRatedAdapter(
    private var mostRated: MutableList<MostRated>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<MostRatedAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view){
        var photo: ImageView
        var ratesTV: TextView
        var nameTV: TextView
        var salesTV: TextView

        init {
            photo = view.findViewById<View>(R.id.photo) as ImageView
            ratesTV = view.findViewById<View>(R.id.ratesTV) as TextView
            nameTV = view.findViewById<View>(R.id.nameTV) as TextView
            salesTV = view.findViewById<View>(R.id.salesTV) as TextView
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.most_rated_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        val  order = mostRated!![position]
//        holder.name.text = order.name
//        holder.price.text = "${order.price} Fcfa"
//        Utils.glide(
//            mActivity,
//        //    order.photo.photoUrl,
//            order.photo,
//            holder.photo,
//            R.drawable.meal_image
//            , false
//        )



    }

    override fun getItemCount(): Int {
        return mostRated!!.size
    }

    fun setItems(data: MutableList<MostRated>?): Unit {
        this.mostRated = data
        notifyDataSetChanged()
    }
    fun removeItem(item :MostRated): Unit {
        this.mostRated!!.remove(item)
        notifyDataSetChanged()
    }

    fun addItem(item :MostRated): Unit {
        this.mostRated!!.add(item)
        notifyDataSetChanged()
    }

    interface Listener{
        fun orderRemoved(MostRated: MostRated)
    }

    companion object {

        private val TAG = "MostRatedAdapter"
    }

}
