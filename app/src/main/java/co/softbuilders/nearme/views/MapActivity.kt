package co.softbuilders.nearme.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import co.softbuilders.nearme.R
import co.softbuilders.nearme.views.home.MenuActivity
import co.softbuilders.nearme.views.service.ArroundSericesAdapter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_map.*

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    val ETOILE_ROUGE = LatLng(6.372364, 2.409665)
    val ZOOM_LEVEL = 16f
    private var mAdapter: ArroundSericesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        initUI()
    }

    private fun initUI() {
        supportActionBar!!.hide()
        val mapFragment : SupportMapFragment? =
            supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        mAdapter = ArroundSericesAdapter(
            MenuActivity.services,
            this
        )
        val mLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recycler_arround_services.apply {
            layoutManager = mLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = mAdapter
        }

        retour.setOnClickListener {
            this.finish()
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap ?: return
        with(googleMap) {
            moveCamera(CameraUpdateFactory.newLatLngZoom(ETOILE_ROUGE, ZOOM_LEVEL))
            addMarker(MarkerOptions().position(ETOILE_ROUGE))
        }
    }
}
