package co.softbuilders.nearme.views.orders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Order
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.utils.Utils


class OrdersAdapter(
    private var orders: MutableList<Order>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<OrdersAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view){
        var overflow: ImageView

        init {
            overflow = view.findViewById<View>(R.id.overflow) as ImageView
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.order_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        val  order = orders!![position]
//        holder.name.text = order.name
//        holder.price.text = "${order.price} Fcfa"
//        Utils.glide(
//            mActivity,
//        //    order.photo.photoUrl,
//            order.photo,
//            holder.photo,
//            R.drawable.meal_image
//            , false
//        )

        holder.overflow.setOnClickListener {
//            OrderActionsDialog(mActivity!!, orders!![position], object : Listener{
//
//                override fun orderRemoved(order: Order) {
//                    removeItem(order)
//                }
//
//            }).show()

            //lets try another option

            OrderActionsPopup(mActivity!!, orders!![position], object : Listener{
                override fun orderRemoved(order: Order) {
                    removeItem(order)
                }
            }, holder.overflow)


        }



    }

    override fun getItemCount(): Int {
        return orders!!.size
    }

    fun setItems(data: MutableList<Order>?): Unit {
        this.orders = data
        notifyDataSetChanged()
    }
    fun removeItem(item :Order): Unit {
        this.orders!!.remove(item)
        notifyDataSetChanged()
    }

    fun addItem(item :Order): Unit {
        this.orders!!.add(item)
        notifyDataSetChanged()
    }

    interface Listener{
        fun orderRemoved(Order: Order)
    }

    companion object {

        private val TAG = "OrderAdapter"
    }

}
