package co.softbuilders.nearme.views.products

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.data.models.TradeFull
import co.softbuilders.nearme.utils.Functions
import co.softbuilders.nearme.views.products.ProductsFragment.FavoriteMealFragment.LaunchPicker
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * @Doc $doc
 */

class AddFavoriteMealDialog(context: Context, trade: Trade, launchPicker: LaunchPicker) : View.OnClickListener {

    private var dialog: AlertDialog? = null
    private lateinit var v: View
    internal var nameET: EditText? = null
    internal var priceET: EditText? = null
    internal var photoTV: TextView? = null
    private var mContext: Context? = null
    private lateinit var trade: Trade
    private lateinit var mDisposable: CompositeDisposable
    private lateinit var launchPicker: ProductsFragment.FavoriteMealFragment.LaunchPicker
    private lateinit var imagePath: String

    init { //inviteEmailPromptNext
        if (dialog == null) {
            mContext = context
            v = LayoutInflater.from(context).inflate(R.layout.dialog_add_favorite_meal, null)
            dialog = AlertDialog.Builder(context!!).setView(v).create()
            dialog!!.setCancelable(false)
            nameET = v.findViewById<EditText>(R.id.name)
            priceET = v.findViewById<EditText>(R.id.price)
            photoTV = v.findViewById<TextView>(R.id.photo)
            this.trade = trade
            v.findViewById<View>(R.id.close_dialog).setOnClickListener(this)
            v.findViewById<View>(R.id.btn_validate).setOnClickListener(this)
            this.launchPicker = launchPicker
            photoTV!!.setOnClickListener {
                launchPicker.launch()
            }

        }
    }


    fun setImagePath(imagePath:String){
        this.imagePath = imagePath
        photoTV!!.text = imagePath
    }

    fun show(): AddFavoriteMealDialog {
        if (dialog != null) {
            dialog!!.show()
        }
        return this
    }

    fun dismiss(): AddFavoriteMealDialog {
        if (dialog != null) {
            dialog!!.dismiss()
        }
        return this
    }

    private lateinit var onClickListener: OnClickListener

    fun setOnClickListner(onClickListner: OnClickListener) {
        this.onClickListener = onClickListner
    }


    override fun onClick(view: View) {

        when (view.id) {
            R.id.close_dialog ->  dialog!!.dismiss()
            R.id.btn_validate -> if (!checkFields()) uploadPhoto()
            else -> {
            }
        }
    }


    private fun uploadPhoto(){
        Functions.showProgress(mContext!!)
        //retrieve image before processing
        val file = Functions.getFileFromBitmap(BitmapFactory.decodeFile(photoTV!!.text.toString()))
        val reqFile = RequestBody.create(MediaType.parse("image/*"), file)
        val body = MultipartBody.Part.createFormData("photo", file?.getName(), reqFile)

        mDisposable = CompositeDisposable()
        try {
            mDisposable.add(
                App.getNetworkService().uploadPhoto(body).subscribeOn(Schedulers.io()).observeOn(
                    AndroidSchedulers.mainThread()
                ).subscribe({ photoUploaded ->
                    Functions.hideProgress()
                    dialog!!.dismiss()
                    Log.e("photo object", Gson().toJson(photoUploaded))
                    //call the second service with the picture uploaded
                    addFavoriteMeal(photoUploaded.id)
                },
                { error ->
                    Functions.hideProgress()
                    dialog!!.dismiss()
                    Functions.toastLong(mContext!!, "Error on uploading picture. Please Retry")
                })
            )
        } catch (ex: Exception) {
            Functions.hideProgress()
            dialog!!.dismiss()
            Functions.toastLong(mContext!!, "Error on uploading picture. Please Retry")
        }
    }


    //make the call to the service to update the commission
    private fun addFavoriteMeal(photoID:String) {
        Functions.showProgress(mContext!!)
        mDisposable = CompositeDisposable()
        try {
            mDisposable.add(
                App.getNetworkService().saveFavoriteMeal(
                   nameET!!.text.toString(), priceET!!.text.toString().toDouble(), this.trade.id, photoID
                ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({ meal ->
                    Functions.hideProgress()
                    dialog!!.dismiss()
                    Log.e("meal added", Gson().toJson(meal))
                    //refresh the view to show change
                    launchPicker.mealAdded(meal)

                },
                { error ->
                    Functions.hideProgress()
                    dialog!!.dismiss()
                    Functions.toastLong(mContext!!, "Erreur lors de l'enregistrement. Veuillez reessayer")
                })
            )
        } catch (ex: Exception) {
            Functions.hideProgress()
            dialog!!.dismiss()
            Functions.toastLong(mContext!!, "Erreur lors de l'enregistrement. Veuillez reessayer")
        }
    }

    fun  checkFields(): Boolean {

         var verif = false;

        if (nameET!!.getText().toString().trim().equals("")) {
            nameET!!.setError(mContext!!.getString(R.string.fill_field));
            verif = true;
        }
        if (priceET!!.getText().toString().trim().equals("")) {
            priceET!!.setError(mContext!!.getString(R.string.fill_field));
            verif = true;
        }
        if (photoTV!!.getText().toString().trim().equals("")) {
            photoTV!!.setError(mContext!!.getString(R.string.fill_field));
            verif = true;
        }

        return verif;
    }

    interface OnClickListener {
        fun onCancel()
//        fun onConfirm(service :ServiceRemote)
    }

    companion object {

        fun Builder(context: Context, trade: TradeFull, launchPicker: LaunchPicker): AddFavoriteMealDialog {
            return AddFavoriteMealDialog(context, trade,launchPicker)
        }
    }
}
