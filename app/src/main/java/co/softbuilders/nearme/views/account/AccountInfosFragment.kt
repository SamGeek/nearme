package co.softbuilders.nearme.views.account

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.data.models.TradeViewModel
import co.softbuilders.nearme.utils.Functions
import co.softbuilders.nearme.utils.Functions.checkEmptyFields
import co.softbuilders.nearme.utils.Functions.checkPhoneNumber
import co.softbuilders.nearme.views.home.MenuActivity
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_account_infos.*
import kotlinx.android.synthetic.main.activity_account_infos.view.*
import kotlinx.android.synthetic.main.fragment_commerce_infos.view.*
import kotlinx.android.synthetic.main.fragment_commerce_infos.view.name
import kotlinx.android.synthetic.main.fragment_commerce_infos.view.tradeType
import kotlinx.android.synthetic.main.fragment_first_step.name
import kotlinx.android.synthetic.main.fragment_first_step.tradePhoneNumber
import kotlinx.android.synthetic.main.fragment_first_step.tradeType
import kotlinx.android.synthetic.main.fragment_proprietaire_infos.*
import kotlinx.android.synthetic.main.fragment_proprietaire_infos.view.*


private const val ARG_TRADE_CONNECTED = "TradeConnected"
private const val ARG_PARAM2 = "param2"


class AccountInfosFragment : Fragment() {

    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            tradeConnected = Gson().fromJson(it.getString(ARG_TRADE_CONNECTED),Trade::class.java)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_account_infos, container, false)
        view.headerText.text = "Infos du compte"
//        if(intent.extras!=null){
//            tradeConnected = Gson().fromJson(intent.extras.getString("tradeConnected"),Trade::class.java)
//        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)

        // Set up the ViewPager with the sections adapter.
        view.container.adapter = mSectionsPagerAdapter

        view.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(view.tabs))
        view.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(view.container))

        view.tabs.addTab(view.tabs.newTab().setText("Commerce"))
        view.tabs.addTab(view.tabs.newTab().setText("Proprietaire"))

        view.overflow.setOnClickListener {
            listener?.onOpenDrawer()
        }


        return view
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            when(position){
                0-> return CommerceFragment.newInstance(0 )
                1-> return ProprietaireFragment.newInstance(1 )
                else-> return CommerceFragment.newInstance(0 )
            }

        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 2
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    class CommerceFragment : Fragment() {
        private lateinit var mDisposable: CompositeDisposable
        private var showPass= true

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(R.layout.fragment_commerce_infos, container, false)

            rootView.name.setText(AccountInfosFragment.tradeConnected.name)
            rootView.tradeType.setText(AccountInfosFragment.tradeConnected.tradeType)
            rootView.tradePhoneNumber.setText(AccountInfosFragment.tradeConnected.tradePhoneNumber)
            rootView.password.setText(AccountInfosFragment.tradeConnected.password)
            rootView.avatar.setText(AccountInfosFragment.tradeConnected.avatarUrl)

            rootView.showPassword.setOnClickListener {
                if(showPass){
                    //we want to show the password by default it is why it set to TRUE
                    showPass = false
                    rootView.password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL
                    rootView.showPassword.setColorFilter(null)
                }else {
                    //we want to hide the password
                    showPass= true
                    rootView.password.inputType = InputType.TYPE_CLASS_TEXT or  InputType.TYPE_TEXT_VARIATION_PASSWORD
                    rootView.showPassword.setColorFilter(ContextCompat.getColor(requireContext(), R.color.lighterGray), android.graphics.PorterDuff.Mode.MULTIPLY);

                }

            }


            //for the profile picture update, we will need some more customizations

            rootView.submit.setOnClickListener {
                //we have to retrieve the updated fields and save them to database through service

                if (checkEmptyFields(name) and checkEmptyFields(tradeType) and checkPhoneNumber(tradePhoneNumber)
                        and checkEmptyFields(rootView.password)) {
                    //all the conditions are now met we can update the fields

                    Functions.showProgress(activity as Context)
                    mDisposable = CompositeDisposable()
                    try {
                        mDisposable.add(
                            App.getNetworkService().updateTrade(AccountInfosFragment.tradeConnected.id,rootView.name.text.toString().trim(),
                                                                    rootView.tradeType.text.toString().trim(), rootView.tradePhoneNumber.text.toString().trim(),
                                                                    rootView.password.text.toString().trim() ).subscribeOn(
                                Schedulers.io()).observeOn(
                                AndroidSchedulers.mainThread()).subscribe({ tradeUpdated ->
                                Functions.hideProgress()
                                Log.e("", Gson().toJson(tradeUpdated))
                                MenuActivity.model.trade.value = tradeUpdated as Trade
                                AccountInfosFragment.tradeConnected = tradeUpdated as Trade
                                //print an update information to the screen
                                Functions.toastLong(requireContext(), "Mise à jour éffectuée avec succès")
                            },
                            { error ->
                                Functions.hideProgress()
                                Functions.toastLong(requireContext(), "Erreur lors de la mise à jour du propriétaire")
                            })
                        )
                    } catch (ex: Exception) {
                        Functions.hideProgress()
                        Functions.toastLong(requireContext(), "Erreur lors de la mise à jour du propriétaire. Veuillez reessayer")
                    }
                }

            }


            return rootView
        }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int): CommerceFragment {
                val fragment = CommerceFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    class ProprietaireFragment : Fragment() {

        private lateinit var mDisposable: CompositeDisposable

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(R.layout.fragment_proprietaire_infos, container, false)

            rootView.traderFirstName.setText(AccountInfosFragment.tradeConnected.traderFirstName)
            rootView.traderLastName.setText(AccountInfosFragment.tradeConnected.traderLastName)
            rootView.traderPhoneNumber.setText(AccountInfosFragment.tradeConnected.traderPhoneNumber)

            rootView.submitOwner.setOnClickListener {
                //we have to retrieve the updated fields and save them to database through service

               if ( checkEmptyFields(traderFirstName) and checkEmptyFields(traderLastName) and checkPhoneNumber(traderPhoneNumber)) {
                   //all the conditions are now met we can update the fields

                   Functions.showProgress(activity as Context)
                   mDisposable = CompositeDisposable()
                   try {
                       mDisposable.add(
                           App.getNetworkService().updateTraderOwner(AccountInfosFragment.tradeConnected.id,rootView.traderFirstName.text.toString().trim(),
                               rootView.traderLastName.text.toString().trim(),rootView.traderPhoneNumber.text.toString().trim() ).subscribeOn(
                               Schedulers.io()).observeOn(
                               AndroidSchedulers.mainThread()).subscribe({ tradeUpdated ->
                               Functions.hideProgress()
                               Log.e("", Gson().toJson(tradeUpdated))

                               MenuActivity.model.trade.value = tradeUpdated
                               AccountInfosFragment.tradeConnected = tradeUpdated
                               //print an update information to the screen
                               Functions.toastLong(requireContext(), "Mise à jour éffectuée avec succès")
                           },
                           { error ->
                               Functions.hideProgress()
                               Functions.toastLong(requireContext(), "Erreur lors de la mise à jour du propriétaire")
                           })
                       )
                   } catch (ex: Exception) {
                       Functions.hideProgress()
                       Functions.toastLong(requireContext(), "Erreur lors de la mise à jour du propriétaire. Veuillez reessayer")
                   }
               }

            }

            return rootView
        }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int): ProprietaireFragment {
                val fragment = ProprietaireFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun onOpenDrawer()
    }

    companion object{
        lateinit var model: TradeViewModel
        lateinit var tradeConnected: Trade
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param tradeConnected The trade connected in string format.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RestaurantFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(tradeConnected: String, param2: String) =
            AccountInfosFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TRADE_CONNECTED, tradeConnected)
                    putString(ARG_PARAM2, param2)
                }
            }
    }




}
