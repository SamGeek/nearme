package co.softbuilders.nearme.views.joinus

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText

import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.utils.Functions
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_third_step.*
import kotlinx.android.synthetic.main.fragment_third_step.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ThirdStepFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ThirdStepFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ThirdStepFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_third_step, container, false)

        view.submit.setOnClickListener {
            //check if code is set and now verify it
            if(Functions.checkEmptyFields(code)) verifyCode()
        }

        return view
    }

    private fun verifyCode() {

        var trade = Gson().fromJson(arguments!!.getString("tradeJson"), Trade::class.java)

        Functions.showProgress(activity as Context)
        mDisposable = CompositeDisposable()
        try {
            mDisposable.add(
                App.getNetworkService().verifyCodeOtp(trade.traderPhoneNumber,code.text.toString().trim()).subscribeOn(
                    Schedulers.io()).observeOn(
                    AndroidSchedulers.mainThread()).subscribe({ tradeSaved ->
                    Functions.hideProgress()
                    Log.e("", Gson().toJson(tradeSaved))
//                    redirect to code step

                    val bundle = Bundle()
                    bundle.putString("tradeJson", Gson().toJson(tradeSaved))
                    listener?.onThirdStepDone(Gson().toJson(tradeSaved))

                },
                { error ->
                    Functions.hideProgress()
                    Functions.toastLong(requireContext(), "Le code renseigné n'est pas correct. Veuillez reessayer")
                })
            )
        } catch (ex: Exception) {
            Functions.hideProgress()
            Functions.toastLong(requireContext(), "Le code renseigné n'est pas correct. Veuillez reessayer")
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun onThirdStepDone(toJson: String)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ThirdStepFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ThirdStepFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
