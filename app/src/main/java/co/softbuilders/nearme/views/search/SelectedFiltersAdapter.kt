package co.softbuilders.nearme.views.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Service


class SelectedFiltersAdapter(
    private var providerList: List<Service>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<SelectedFiltersAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        /*var providerName: TextView*/
        var filterTV: TextView

        init {
            /*providerName = view.findViewById<View>(R.id.providerName) as TextView
            providerImage = view.findViewById<View>(R.id.providerImage) as ImageView
            view.findViewById<View>(R.id.provider).setOnClickListener(this)*/
            filterTV = view.findViewById<View>(R.id.filterTV) as TextView
        }

        override fun onClick(view: View) {
//            (mActivity as MenuActivity).loadServiceInterface(providerList?.get(adapterPosition)?.serviceName)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.filtre_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       val service = providerList!![position]
        /*holder.providerName.text = serviceName
        holder.providerImage.setImageDrawable(mActivity.resources.getDrawable(serviceImageId))*/
        holder.filterTV.text = service.serviceName
    }

    override fun getItemCount(): Int {
        return providerList!!.size
    }

    fun setItems(data: List<Service>): Unit {
        this.providerList = data
        notifyDataSetChanged()
    }


    companion object {

        private val TAG = "ServiceAdapter"
    }

}
