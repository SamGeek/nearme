package co.softbuilders.nearme.views.home

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.Menu
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.*
import co.softbuilders.nearme.utils.Functions
import co.softbuilders.nearme.utils.Utils
import co.softbuilders.nearme.views.FavorisFragment
import co.softbuilders.nearme.views.HomeActivity
import co.softbuilders.nearme.views.MapActivity
import co.softbuilders.nearme.views.account.AccountInfosFragment
import co.softbuilders.nearme.views.orders.OrdersFragment
import co.softbuilders.nearme.views.products.ProductsFragment
import co.softbuilders.nearme.views.search.SearchActivity
import co.softbuilders.nearme.views.service.ChooseServiceFragment
import co.softbuilders.nearme.views.service.RestaurantFragment
import co.softbuilders.nearme.views.service.ServiceDetailsFragment
import co.softbuilders.nearme.views.statistics.StatisticsFragment
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.app_bar_menu.*
import kotlinx.android.synthetic.main.content_menu.*
import kotlinx.android.synthetic.main.nav_header_menu.view.*
import kotlinx.android.synthetic.main.nav_header_menu.view.drawerTopHeader


class MenuActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    RestaurantFragment.OnFragmentInteractionListener,
    ChooseServiceFragment.OnFragmentInteractionListener,
    FavorisFragment.OnFragmentInteractionListener,
    ServiceDetailsFragment.OnFragmentInteractionListener,
    //All the new involes for the drawer fragments
    StatisticsFragment.OnFragmentInteractionListener,
    OrdersFragment.OnFragmentInteractionListener,
    ProductsFragment.OnFragmentInteractionListener,
    AccountInfosFragment.OnFragmentInteractionListener{


    lateinit var tradeConnected: Trade

    override fun onFragmentInteraction(uri: Uri) {

    }

    override fun onOpenDrawer() {
        drawer_layout.openDrawer(GravityCompat.START)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        initUI()
    }

    private fun initUI() {
        setSupportActionBar(toolbar)
        supportActionBar!!.hide()

        model = ViewModelProviders.of(this).get(TradeViewModel::class.java)

        // Create the observer which updates the UI.
        val tradeObserver = Observer<Trade> { newTrade ->
            nav_view.getHeaderView(0).tradeName.text = newTrade.name
            nav_view.getHeaderView(0).tradeType.text = newTrade.tradeType
            Utils.glide(
                this,
                newTrade.avatarUrl,
                nav_view.getHeaderView(0).profile,
                R.drawable.default_profile
                , true
            )
            //update the connected trade with the newest value
            tradeConnected = newTrade
        }

        model.trade.observe(this, tradeObserver)

        if (intent.extras != null && intent.extras.getString("tradeConnected")!=null) {
            tradeConnected = Gson().fromJson(intent.extras.getString("tradeConnected"), Trade::class.java)
            model.trade.value =  tradeConnected
        }

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment? ?: return

        // Set up Action Bar
        val navController = host.navController

        setupNavigationMenu(navController)

        //enable asap
        /*navController.addOnDestinationChangedListener { _, destination, _ ->
            val dest: String = try {
                resources.getResourceName(destination.id)
            } catch (e: Resources.NotFoundException) {
                Integer.toString(destination.id)
            }

            Toast.makeText(
                this@MenuActivity, "Navigated to $dest",
                Toast.LENGTH_SHORT
            ).show()
        }*/

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)


        if (::tradeConnected.isInitialized) {
            nav_view.getHeaderView(0).tradeName.text = tradeConnected.name
            nav_view.getHeaderView(0).tradeType.text = tradeConnected.tradeType
            Utils.glide(
                this,
                tradeConnected.avatarUrl,
                nav_view.getHeaderView(0).profile,
                R.drawable.default_profile
                , true
            )
        }

        menu_recycler.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
        }




        // we only add logout if a trade is actually connected
        if (Functions.isTradeConnected()){
            //here we are doing everything for connected people
            if (!appMenu.contains(disconnectMenu)) appMenu.add( disconnectMenu  )


            hideRelative.visibility = View.GONE
            //change the color of the status bar
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.setStatusBarColor(getResources().getColor(R.color.colorBlueDark))
            }

            nav_view.getHeaderView(0).drawerTopHeader.setBackgroundResource(R.drawable.joinusheader)
            loadAppropriateMenuContent("Mes statistiques")

        }

        if (Functions.isTradeConnected()){

        }

        menu_recycler.adapter = MenuAdapter(
            appMenu,
            this@MenuActivity
        )

//        updateDrawerInfos

        bottomBar.setOnTabSelectListener { tabId ->
            if (tabId == R.id.menu) {
                // The tab with id R.id.tab_favorites was selected,
                // change your content accordingly.
                /*replaceFragment(
                    ChooseServiceFragment.newInstance("",""),R.id.container)*/
                findNavController(R.id.nav_host_fragment).navigate(R.id.chooseServiceFragment)
            }
            if (tabId == R.id.favoris) {
                // The tab with id R.id.tab_favorites was selected,
                // change your content accordingly.

                //twerk this later
                findNavController(R.id.nav_host_fragment).navigate(R.id.restaurantFragment)
//                findNavController(R.id.nav_host_fragment).navigate(R.id.action_chooseServiceFragment_to_favorisFragment)
            }
            if (tabId == R.id.recherche) {
                // The tab with id R.id.tab_favorites was selected,
                // change your content accordingly.
                startActivity(Intent(this, SearchActivity::class.java))
            }
            if (tabId == R.id.map) {
                // The tab with id R.id.tab_favorites was selected,
                // change your content accordingly.
                startActivity(Intent(this, MapActivity::class.java))
            }

        }

        //a etudier
        if (intent.extras!=null && intent.extras.getString("openMode")!=null ){
            if(intent.extras.getString("openMode").equals("resto_liste") ) findNavController(R.id.nav_host_fragment).navigate(R.id.action_chooseServiceFragment_to_restaurantFragment)
        }

    }

    private fun setupNavigationMenu(navController: NavController) {
        // TODO STEP 9.4 - Use NavigationUI to set up a Navigation View
//        // In split screen mode, you can drag this view out from the left
//        // This does NOT modify the actionbar
//        val sideNavView = findViewById<NavigationView>(R.id.nav_view)
//        sideNavView?.setupWithNavController(navController)
        // TODO END STEP 9.4
    }

    override fun onResume() {
        super.onResume()
       // bottomBar.selectTabAtPosition(0)
    }


    public fun loadServiceInterface(selectedService: String?) {
//        Functions.toastLong(this, selectedService.toString())
        when (selectedService) {
            "Restaurants" -> replaceFragment(RestaurantFragment.newInstance("", ""), R.id.container)
            else -> replaceFragment(RestaurantFragment.newInstance("", ""), R.id.container)
        }
    }


    public fun loadServiceDetails(selectedService: Service?) {
//        replaceFragment(ServiceDetailsFragment.newInstance("", ""), R.id.container)
    }

    public fun loadAppropriateMenuContent(selectedMenu: String?) {

//        Functions.toastLong(this, selectedMenu.toString())

        //make sure that a user is connected before showing any appropriate menu
        if (::tradeConnected.isInitialized) {
            when (selectedMenu) {
                "Infos du compte" -> {
//                    val intent = Intent(this@MenuActivity, AccountInfosFragment::class.java)
//                    intent.putExtra("tradeConnected", Gson().toJson(tradeConnected))
//                    startActivity(intent)
                    replaceFragment(AccountInfosFragment.newInstance(Gson().toJson(tradeConnected), ""), R.id.conteneur)
                }
                "Mes produits" -> {
                    replaceFragment(ProductsFragment.newInstance(Gson().toJson(tradeConnected), ""), R.id.conteneur)
                }
                "Mes commandes/réservations" -> {
                    replaceFragment(OrdersFragment.newInstance("", ""), R.id.conteneur)
                }
                "Mes statistiques" -> {
                    replaceFragment(StatisticsFragment.newInstance("", ""), R.id.conteneur)

                }
                "Déconnexion" -> {
                    App.appPreferences.tradeConnected = null
                    appMenu.remove( disconnectMenu  )
                    startActivity(Intent(this@MenuActivity, HomeActivity::class.java))
                }
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    companion object {

        lateinit var model: TradeViewModel
        val disconnectMenu =  MenuItemView(R.drawable.logout, "Déconnexion")

        var services = mutableListOf<Service>(
            Service(R.drawable.restaurants, "Restaurants"),
            Service(R.drawable.lounge, "Lounge & Bars"),
            Service(R.drawable.boite_nuit, "Boites de nuit"),
            Service(R.drawable.hotels, "Hotels"),
            Service(R.drawable.plages, "Plages"),
            Service(R.drawable.sport, "Sport & Détente"),
            Service(R.drawable.culture, "Espaces culturels"),
            Service(R.drawable.beaute_soins, "Beauté & Soins"),
            Service(R.drawable.pret_porter, "Pret à porter")
        )

        var appMenu = mutableListOf<MenuItemView>(
            MenuItemView(R.drawable.stat, "Mes statistiques"),
            MenuItemView(R.drawable.commandes, "Mes commandes/réservations"),
            MenuItemView(R.drawable.produits, "Mes produits"),
            MenuItemView(R.drawable.premium, "Mon premium"),
            MenuItemView(R.drawable.compte, "Infos du compte")
        )

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    public inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().commit()
    }

    public fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction { add(frameId, fragment) }
    }


    public fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction { replace(frameId, fragment) }
    }

    fun setActionBarTitle(title: String) {
        supportActionBar!!.title = title
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
