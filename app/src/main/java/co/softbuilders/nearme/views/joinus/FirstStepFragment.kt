package co.softbuilders.nearme.views.joinus

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Trade
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_first_step.*
import kotlinx.android.synthetic.main.fragment_first_step.view.*



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FirstStepFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FirstStepFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FirstStepFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_first_step, container, false)

        view.submit.setOnClickListener {
           if (!checkFields()){
//               var bundle = bundleOf("amount" to )

               val trade = Trade()
               trade.name  = name.text.toString().trim()
               trade.tradeType = tradeType.text.toString().trim()
               trade.tradePhoneNumber= "+229${tradePhoneNumber.text.toString().trim()}"
               trade.password = password.text.toString().trim()
               trade.avatarUrl = avatar.text.toString().trim()
               val bundle = Bundle()
               bundle.putString("tradeJson", Gson().toJson(trade))

               listener?.onFirstStepDone(bundle)
           }
        }

        view.avatar.setOnClickListener {
            ImagePicker.create(this@FirstStepFragment)
                .returnMode(ReturnMode.ALL) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(true) // set folder mode (false by default)
                .single()
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select")
                .toolbarDoneButtonText("DONE") // done button text
                .start(0) // image selection title
        }


        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val images = ImagePicker.getImages(data)
        if (images != null && !images.isEmpty()) {
            avatar.setText(images[0].path)
//            imageView.setImageBitmap(BitmapFactory.decodeFile(images[0].path))
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun checkFields(): Boolean {

        var verif = false

        if (name.getText().toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
            name.setError(getString(R.string.fill_field))
            verif = true
        }

        if (tradeType.getText().toString().trim({ it <= ' ' }).equals("", ignoreCase = true)) {
            tradeType.setError(getString(R.string.fill_field))
            verif = true
        }

        if (tradePhoneNumber.getText().toString().trim({ it <= ' ' }).equals("", ignoreCase = true)
            || tradePhoneNumber.getText().toString().trim({ it <= ' ' }).length < 8) {
            tradePhoneNumber.setError("Veuillez entrer un numéro d'au moins 8 chiffres")
            verif = true
        }

        if (password.getText().toString().trim({ it <= ' ' }).equals("", ignoreCase = true)) {
            password.setError(getString(R.string.fill_field))
            verif = true
        }

        if (avatar.getText().toString().trim({ it <= ' ' }).equals("", ignoreCase = true)) {
            avatar.setError(getString(R.string.fill_field))
            verif = true
        }

        return verif
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun onFirstStepDone(bundle: Bundle)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FirstStepFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FirstStepFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
