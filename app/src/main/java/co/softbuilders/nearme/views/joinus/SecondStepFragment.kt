package co.softbuilders.nearme.views.joinus

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.utils.Functions
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_second_step.*
import kotlinx.android.synthetic.main.fragment_second_step.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SecondStepFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SecondStepFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SecondStepFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mDisposable: CompositeDisposable
    private var isClickable = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_second_step, container, false)
        view.submit.setOnClickListener {
            //make the call to create the trade
            if (!checkFields() && isClickable) saveTrade()
        }

        return view
    }


    private fun saveTrade() {
        isClickable = false
        Functions.showProgress(activity as Context)
        var tradeToSave = Gson().fromJson(arguments!!.getString("tradeJson"), Trade::class.java)

        val file = Functions.getFileFromBitmap(BitmapFactory.decodeFile(tradeToSave.avatarUrl))
        val reqFile = RequestBody.create(MediaType.parse("image/*"), file)
        val body = MultipartBody.Part.createFormData("avatar", file?.getName(), reqFile)

        tradeToSave.traderFirstName = traderFirstName.text.toString().trim()
        tradeToSave.traderLastName = traderLastName.text.toString().trim()
        tradeToSave.traderPhoneNumber = "+229${traderPhoneNumber.text.toString().trim()}"


        val nameField = RequestBody.create(MediaType.parse("text/plain"), tradeToSave.name)
        val tradeTypeField = RequestBody.create(MediaType.parse("text/plain"), tradeToSave.tradeType)
        val tradePhoneNumberField = RequestBody.create(MediaType.parse("text/plain"), tradeToSave.tradePhoneNumber)
        val passwordField = RequestBody.create(MediaType.parse("text/plain"), tradeToSave.password)
        val traderFirstNameField = RequestBody.create(MediaType.parse("text/plain"), tradeToSave.traderFirstName)
        val traderLastNameField = RequestBody.create(MediaType.parse("text/plain"), tradeToSave.traderLastName)
        val traderPhoneNumberField = RequestBody.create(MediaType.parse("text/plain"), tradeToSave.traderPhoneNumber)

        mDisposable = CompositeDisposable()
        try {
            mDisposable.add(
                App.getNetworkService().saveTrade(
                         nameField, tradeTypeField,
                    tradePhoneNumberField,passwordField , traderFirstNameField,
                    traderLastNameField  ,traderPhoneNumberField,body
                )
                .subscribeOn(
                    Schedulers.io()).observeOn(
                    AndroidSchedulers.mainThread()).subscribe({ tradeSaved ->
                    Functions.hideProgress()
                        isClickable = true
                    Log.e("", Gson().toJson(tradeSaved))
//                    redirect to code step

                    val bundle = Bundle()
                    bundle.putString("tradeJson", Gson().toJson(tradeSaved))
                    listener?.onSecondStepDone(bundle)

                },
                { error ->
                    isClickable = true
                    Functions.hideProgress()
                    val erreur =  error as HttpException
                    if(erreur.code() == 404){
                        Functions.toastLong(requireContext(), "Ce numero de proprietaire existe déjà, veuillez en choisir un autre")
                    }else {
                        Functions.toastLong(requireContext(), "Erreur lors de l'ajout de votre commerce. Veuillez reessayer")
                    }
                })
            )
        } catch (ex: Exception) {
            isClickable = true
            Functions.hideProgress()
            Functions.toastLong(requireContext(), "Erreur lors de l'ajout de votre commerce. Veuillez reessayer")
        }
    }


    fun checkFields(): Boolean {

        var verif = false

        if (traderFirstName.getText().toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
            traderFirstName.setError(getString(R.string.fill_field))
            verif = true
        }

        if (traderLastName.getText().toString().trim({ it <= ' ' }).equals("", ignoreCase = true)) {
            traderLastName.setError(getString(R.string.fill_field))
            verif = true
        }

        if (traderPhoneNumber.getText().toString().trim({ it <= ' ' }).equals("", ignoreCase = true)
            || traderPhoneNumber.getText().toString().trim({ it <= ' ' }).length < 8) {
            traderPhoneNumber.setError("Veuillez entrer un numéro d'au moins 8 chiffres")
            verif = true
        }

        return verif
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun onSecondStepDone(tradeSaved: Bundle)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SecondStepFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SecondStepFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
