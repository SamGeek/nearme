package co.softbuilders.nearme.views.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Service


class ServicesAdapter(
    private val providerList: List<Service>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<ServicesAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        var providerName: TextView
        var providerImage: ImageView

        init {
            providerName = view.findViewById<View>(R.id.providerName) as TextView
            providerImage = view.findViewById<View>(R.id.providerImage) as ImageView

            view.findViewById<View>(R.id.provider).setOnClickListener{
                Navigation.findNavController(mActivity,R.id.nav_host_fragment)
                    .navigate(ChooseServiceFragmentDirections.actionChooseServiceFragmentToRestaurantFragment(adapterPosition))
            }
        }

        override fun onClick(view: View) {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.provider_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val (serviceImageId, serviceName) = providerList!![position]
        holder.providerName.text = serviceName
        holder.providerImage.setImageDrawable(mActivity.resources.getDrawable(serviceImageId))
    }

    override fun getItemCount(): Int {
        return providerList!!.size
    }

    companion object {

        private val TAG = "ServiceAdapter"
    }

}
