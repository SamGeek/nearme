package co.softbuilders.nearme.views.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.utils.Functions
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {


    private lateinit var mDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initUI()
    }

    private fun initUI() {
        submit.setOnClickListener {
            if (checkFields()) loginTrade()
        }
    }

    private fun loginTrade() {
        Functions.showProgress(this)
        mDisposable = CompositeDisposable()
        try {
            mDisposable.add(
                App.getNetworkService().traderLogin( "+229${traderPhoneNumber.text.toString()}",password.text.toString()).subscribeOn(
                    Schedulers.io()).observeOn(
                    AndroidSchedulers.mainThread()).subscribe({ tradeSaved ->
                    Functions.hideProgress()
                    Log.e("", Gson().toJson(tradeSaved))

                    //save the connected trade to preferences
                    App.appPreferences.tradeConnected = tradeSaved
                    startActivity(Intent(this@LoginActivity, MenuActivity::class.java).putExtra("tradeConnected",Gson().toJson(tradeSaved)))
                    this.finish()

                },
                { error ->
                    Functions.hideProgress()
                    Functions.toastLong(this, "Veuillez verifier vos identifiants, puis reessayez")
                })
            )
        } catch (ex: Exception) {
            Functions.hideProgress()
            Functions.toastLong(this, "Veuillez verifier vos identifiants, puis reessayez")
        }
    }



    fun checkFields(): Boolean {
        var verif = true
        verif = Functions.checkEmptyFields(traderPhoneNumber);
        verif = Functions.checkEmptyFields(password)
        return verif
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }
}
