package co.softbuilders.nearme.views.search

import android.content.Intent
import android.os.Bundle
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Service
import co.softbuilders.nearme.views.MapActivity
import co.softbuilders.nearme.views.home.MenuActivity
import kotlinx.android.synthetic.main.activity_search.*


class SearchActivity : AppCompatActivity() {

    private var mAdapter: FilterItemAdapter? = null
    private var filtersAdapter: SelectedFiltersAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        initUI()
    }

    private fun initUI() {
        supportActionBar!!.hide()

        mAdapter = FilterItemAdapter(
            MenuActivity.services,
            this,
            object : OnFilterItemChooseListener{
                override fun onEnable(service: List<Service>, editedPosition:Int) {
                    filtersAdapter!!.setItems(service)
                }

                override fun onDisable(service: List<Service>, editedPosition:Int) {
                    filtersAdapter!!.setItems(service)
                }
            }
        )
        val mLayoutManager = GridLayoutManager(this, 2)
        recycler_filter_items.apply {
            layoutManager = mLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = mAdapter
        }

        filtersAdapter = SelectedFiltersAdapter(
            mutableListOf(),
            this
        )

        val filterLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        choosen_filters.apply {
            layoutManager = filterLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = filtersAdapter
        }

        retour.setOnClickListener {
            this.finish()
        }

        searchBtn.setOnClickListener {
//            this.finish()
            when(rg_display_type.checkedRadioButtonId){
                R.id.liste_rb -> {
                    startActivity(Intent(this@SearchActivity, MenuActivity::class.java).putExtra("openMode","resto_liste"))
                }
                R.id.map_rb -> {
                    startActivity(Intent(this@SearchActivity, MapActivity::class.java))
                }
            }

        }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val value = progress * (seekBar!!.getWidth() - 2 * seekBar!!.getThumbOffset()) / seekBar.getMax()
                seekBarText.setText("${progress} Km")
                seekBarText.setX(seekBar!!.getX() + value + seekBar.getThumbOffset() / 2)
                //textView.setY(100); just added a value set this properly using screen with height aspect ratio , if you do not set it by default it will be there below seek bar
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

    }

    interface OnFilterItemChooseListener {
        fun onEnable(service :List<Service>, editedPosition:Int)
        fun onDisable(service :List<Service>, editedPosition:Int)
    }
}
