package co.softbuilders.nearme.views.orders

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Order
import io.reactivex.disposables.CompositeDisposable


/**
 * @Doc $doc
 */

class OrderActionsDialog(ctx: Context, order: Order, listener: OrdersAdapter.Listener) : View.OnClickListener {

    //perhaps we need a lsitener to refresh the recycler after removing an item
    private var dialog: AlertDialog? = null
    private lateinit var v: View
    private var mContext: Context? = null
    private lateinit var order: Order
    private lateinit var listener: OrdersAdapter.Listener
    private lateinit var mDisposable: CompositeDisposable
    private val TAG: String = this::class.java.getSimpleName()



    init {
        if (dialog == null) {
            mContext = ctx
            v = LayoutInflater.from(ctx).inflate(R.layout.dialog_order_actions, null)
            dialog = AlertDialog.Builder(ctx).setView(v).create()
            dialog!!.setCancelable(false)
            v.findViewById<View>(R.id.close_dialog).setOnClickListener(this)
            v.findViewById<View>(R.id.callClient).setOnClickListener(this)
            v.findViewById<View>(R.id.deleteOrder).setOnClickListener(this)
            this.order =  order
            this.listener = listener
        }
    }


    fun show(): OrderActionsDialog {
        if (dialog != null) {
            dialog!!.show()
        }
        return this
    }

    fun dismiss(): OrderActionsDialog {
        if (dialog != null) {
            dialog!!.dismiss()
        }
        return this
    }

    override fun onClick(view: View) {

        when (view.id) {
            R.id.close_dialog ->  dialog!!.dismiss()
            R.id.callClient ->  {
//                var callIntent = Intent(Intent.ACTION_CALL)
//                callIntent.setData(Uri.parse("tel:"+order.phoneNumber));//change the number
//                mContext!!.startActivity(callIntent)
//                Log.e("Le tag d'appel : ",TAG)
                callPhoneNumber()
            }
            R.id.deleteOrder -> {
                //deleteOrder()
                Log.e("Le tag de suppression",TAG)
            }
            else -> {
            }
        }
    }

    fun callPhoneNumber() {
        try {
            if (Build.VERSION.SDK_INT > 22) {
                if (ActivityCompat.checkSelfPermission(
                        mContext!!,
                        Manifest.permission.CALL_PHONE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    ActivityCompat.requestPermissions(
                        mContext as Activity,
                        arrayOf(Manifest.permission.CALL_PHONE),
                        101
                    )
                    return
                }
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:${order.phoneNumber}" )
                mContext!!.startActivity(callIntent)
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:${order.phoneNumber}")
                mContext!!.startActivity(callIntent)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String?>?,
        grantResults: IntArray
    ) {
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callPhoneNumber()
            } else {
                Log.e(TAG, "Permission not Granted")
            }
        }
    }

    fun deleteOrder(){
        listener.orderRemoved(order)
    }

    companion object {

        fun Builder(ctx: Context, order: Order, listener: OrdersAdapter.Listener): OrderActionsDialog {
            return OrderActionsDialog(ctx, order, listener)
        }
    }
}
