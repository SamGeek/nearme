package co.softbuilders.nearme.views.products

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.data.models.TradeFull
import co.softbuilders.nearme.data.models.TradeViewModel
import co.softbuilders.nearme.utils.Functions
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_products.view.*
import kotlinx.android.synthetic.main.fragment_favorite_meal.*
import kotlinx.android.synthetic.main.fragment_favorite_meal.view.*
import kotlinx.android.synthetic.main.fragment_product_menu.*
import kotlinx.android.synthetic.main.fragment_product_menu.view.*


private const val ARG_TRADE_CONNECTED = "TradeConnected"
private const val ARG_PARAM2 = "param2"


class ProductsFragment : Fragment() {

    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            tradeConnected = Gson().fromJson(it.getString(ARG_TRADE_CONNECTED),Trade::class.java)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.activity_products, container, false)
        view.headerText.text = "Mes Produits"
//        if(intent.extras!=null){
//            tradeConnected = Gson().fromJson(intent.extras.getString("tradeConnected"), Trade::class.java)
//        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)

        // Set up the ViewPager with the sections adapter.
        view.container.adapter = mSectionsPagerAdapter

        view.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(view.tabs))
        view.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(view.container))

        view.tabs.addTab(view.tabs.newTab().setText("Plats préférés"))
        view.tabs.addTab(view.tabs.newTab().setText("Menu"))

        model = ViewModelProviders.of(this).get(TradeViewModel::class.java)

        view.overflow.setOnClickListener {
            listener?.onOpenDrawer()
        }

        return view
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            when(position){
                0-> return FavoriteMealFragment.newInstance(0 )
                1-> return ProductMenuFragment.newInstance(1 )
                else-> return FavoriteMealFragment.newInstance(0 )
            }

        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 2
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    class FavoriteMealFragment : Fragment() {

        private var mAdapter: FavoriteMealAdapter?=null
        private lateinit var mDisposable: CompositeDisposable
        private lateinit var favoriteMeals : MutableList<TradeFull.FavoriteMeal>

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(R.layout.fragment_favorite_meal, container, false)

            favoriteMeals = mutableListOf()

            mAdapter = FavoriteMealAdapter(
                favoriteMeals,
                activity as AppCompatActivity
            )
            val mLayoutManager = LinearLayoutManager(context)
            rootView.recycler_favorite_meal.apply {
                layoutManager = mLayoutManager
                itemAnimator = DefaultItemAnimator()
                adapter = mAdapter
            }


            fetchItems()

            rootView.addFavoriteMeal.setOnClickListener {
               dialog=  AddFavoriteMealDialog(context!!, tradeConnected, object : LaunchPicker{
                   override fun mealAdded(meal: TradeFull.FavoriteMeal?) {
                        mAdapter!!.addItem(meal)
                       emptyMeals.visibility = View.GONE
                       recycler_favorite_meal.visibility = View.VISIBLE
                   }

                   override fun launch() {
                        ImagePicker.create(this@FavoriteMealFragment )
                            .returnMode(ReturnMode.ALL) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                            .folderMode(true) // set folder mode (false by default)
                            .single()
                            .toolbarFolderTitle("Folder") // folder selection title
                            .toolbarImageTitle("Tap to select")
                            .toolbarDoneButtonText("DONE") // done button text
                            .start(0) // image selection title
                    }
                }).show()
            }
            return rootView
        }

        private fun fetchItems() {
            Functions.showProgress(activity as Context)
            mDisposable = CompositeDisposable()
            try {
                mDisposable.add(
                    App.getNetworkService().getFavoriteMealsByTradeID(tradeConnected.id)
                        .subscribeOn(
                            Schedulers.io()).observeOn(
                            AndroidSchedulers.mainThread()).subscribe({ favoriteMeals ->
                            Functions.hideProgress()
                            Log.e("", Gson().toJson(favoriteMeals))

                            mAdapter!!.setItems(favoriteMeals)

                            if(favoriteMeals.isEmpty()){
                                emptyMeals.visibility = View.VISIBLE
                                recycler_favorite_meal.visibility = View.GONE
                            }else{
                                emptyMeals.visibility = View.GONE
                                recycler_favorite_meal.visibility = View.VISIBLE
                            }

                            model.favoriteMealLoaded.value =  true

                        },
                        { error ->
                            Functions.hideProgress()
                            Functions.toastLong(requireContext(), "Erreur lors de la recuperation des plats preferes")
                        })
                )
            } catch (ex: Exception) {
                Functions.hideProgress()
                Functions.toastLong(requireContext(), "Erreur lors de la recuperation des plats preferes")
            }
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            val images = ImagePicker.getImages(data)
            if (images != null && !images.isEmpty()) {
                //use the listener to give the image to the dialog
                Functions.toastLong(context!!, images[0].path)
//                avatar.setText(images[0].path)
                dialog.setImagePath(images[0].path)
            }
            super.onActivityResult(requestCode, resultCode, data)
        }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"
            lateinit var dialog : AddFavoriteMealDialog

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int): FavoriteMealFragment {
                val fragment = FavoriteMealFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }

        interface LaunchPicker{
            fun launch()
            fun mealAdded(meal: TradeFull.FavoriteMeal?)
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    class ProductMenuFragment : Fragment() {

        private var mAdapter: ProductMenuAdapter?=null
        private lateinit var mDisposable: CompositeDisposable
        private lateinit var menus : MutableList<TradeFull.Menu>

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(R.layout.fragment_product_menu, container, false)

            val mealObserver = Observer<Boolean> { favoriteMealLoaded ->
                //update the connected trade with the newest value
                if (favoriteMealLoaded) fetchItems()
            }

            model.favoriteMealLoaded.observe(this, mealObserver)

            menus = mutableListOf()
            mAdapter = ProductMenuAdapter(
                menus,
                activity as AppCompatActivity
            )
            val mLayoutManager = LinearLayoutManager(context)
            rootView.recycler_product_menu.apply {
                layoutManager = mLayoutManager
                itemAnimator = DefaultItemAnimator()
                adapter = mAdapter
            }

            rootView.addMenu.setOnClickListener {
                AddMenuDialog(context!!, tradeConnected, object : Listener{
                    override fun menuAdded(menu: TradeFull.Menu?) {
                        mAdapter!!.addItem(menu)
                        emptyMenus.visibility = View.GONE
                        recycler_product_menu.visibility = View.VISIBLE
                    }

                }).show()
            }

            return rootView
        }


        private fun fetchItems() {
            Functions.showProgress(activity as Context)
            mDisposable = CompositeDisposable()
            try {
                mDisposable.add(
                    App.getNetworkService().getMenusByTradeID(tradeConnected.id)
                        .subscribeOn(
                            Schedulers.io()).observeOn(
                            AndroidSchedulers.mainThread()).subscribe({ menus ->
                            Functions.hideProgress()
                            Log.e("", Gson().toJson(menus))

                            mAdapter!!.setItems(menus)

                            if(menus.isEmpty()){
                                emptyMenus.visibility = View.VISIBLE
                                recycler_product_menu.visibility = View.GONE
                            }else{
                                emptyMenus.visibility = View.GONE
                                recycler_product_menu.visibility = View.VISIBLE
                            }

                        },
                        { error ->
                            Functions.hideProgress()
                            Functions.toastLong(requireContext(), "Erreur lors de la recuperation des menus")
                        })
                )
            } catch (ex: Exception) {
                Functions.hideProgress()
                Functions.toastLong(requireContext(), "Erreur lors de la recuperation des menus")
            }
        }

        interface Listener{
            fun menuAdded(menu: TradeFull.Menu?)
        }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int): ProductMenuFragment {
                val fragment = ProductMenuFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }




    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun onOpenDrawer()
    }

    companion object{
        lateinit var model: TradeViewModel
        lateinit var tradeConnected: Trade
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RestaurantFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(tradeConnected: String, param2: String) =
            ProductsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TRADE_CONNECTED, tradeConnected)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
