package co.softbuilders.nearme.views.service

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.Service
import co.softbuilders.nearme.views.home.MenuActivity
import com.google.gson.Gson


class OtherServiceAdapter(
    private val providerList: List<Service>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<OtherServiceAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
       /* var providerName: TextView
        var providerImage: ImageView*/

        init {
           /* providerName = view.findViewById<View>(R.id.providerName) as TextView
            providerImage = view.findViewById<View>(R.id.providerImage) as ImageView*/
            view.findViewById<View>(R.id.restaurant).setOnClickListener{
                Navigation.findNavController(mActivity,R.id.nav_host_fragment)
                    .navigate(R.id.action_restaurantFragment_to_serviceDetailsFragment)
            }
        }

        override fun onClick(view: View) {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.restaurant_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val (serviceImageId, serviceName) = providerList!![position]
        /*holder.providerName.text = serviceName
        holder.providerImage.setImageDrawable(mActivity.resources.getDrawable(serviceImageId))*/
    }

    override fun getItemCount(): Int {
        return providerList!!.size
    }

    companion object {

        private val TAG = "ServiceAdapter"
    }

}
