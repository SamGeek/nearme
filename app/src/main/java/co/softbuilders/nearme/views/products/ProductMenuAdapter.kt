package co.softbuilders.nearme.views.products

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.models.DummyItem
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.data.models.TradeFull


class ProductMenuAdapter(
    private var menus: MutableList<TradeFull.Menu>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<ProductMenuAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view){
        var name: TextView
        var price: TextView

        init {
            name = view.findViewById<View>(R.id.name) as TextView
            price = view.findViewById<View>(R.id.price) as TextView
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.product_menu_item, parent, false)
        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val menu = menus!![position]
        holder.name.text = menu.name
        holder.price.text = "${menu.price} Fcfa"
    }

    override fun getItemCount(): Int {
        return menus!!.size
    }

    fun setItems(data: MutableList<TradeFull.Menu>?): Unit {
        this.menus = data
        notifyDataSetChanged()
    }

    fun addItem(item: TradeFull.Menu?): Unit {
        this.menus!!.add(item!!)
        notifyDataSetChanged()
    }

    companion object {

        private val TAG = "ProductMenuAdapter"
    }

}
