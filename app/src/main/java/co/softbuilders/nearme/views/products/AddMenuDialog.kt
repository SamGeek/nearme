package co.softbuilders.nearme.views.products

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.utils.Functions
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * @Doc $doc
 */

class AddMenuDialog(ctx: Context, trade: Trade, listener: ProductsFragment.ProductMenuFragment.Listener) : View.OnClickListener {

    private var dialog: AlertDialog? = null
    private lateinit var v: View
    internal var name: EditText? = null
    internal var category: EditText? = null
    internal var price: EditText? = null
    private var mContext: Context? = null
    private lateinit var trade: Trade
    private lateinit var listener: ProductsFragment.ProductMenuFragment.Listener
    private lateinit var mDisposable: CompositeDisposable

    init { //inviteEmailPromptNext
        if (dialog == null) {
            mContext = ctx
            v = LayoutInflater.from(ctx).inflate(R.layout.dialog_add_menu, null)
            dialog = AlertDialog.Builder(ctx).setView(v).create()
            dialog!!.setCancelable(false)
            name = v.findViewById<EditText>(R.id.name)
            category = v.findViewById<EditText>(R.id.category)
            price = v.findViewById<EditText>(R.id.price)
            v.findViewById<View>(R.id.close_dialog).setOnClickListener(this)
            v.findViewById<View>(R.id.btn_validate).setOnClickListener(this)
            this.trade =  trade
            this.listener = listener
        }
    }


    fun show(): AddMenuDialog {
        if (dialog != null) {
            dialog!!.show()
        }
        return this
    }

    fun dismiss(): AddMenuDialog {
        if (dialog != null) {
            dialog!!.dismiss()
        }
        return this
    }


    override fun onClick(view: View) {

        when (view.id) {
            R.id.close_dialog ->  dialog!!.dismiss()
            R.id.btn_validate -> if (!checkFields()) addMenu()
            else -> {
            }
        }
    }

    //make the call to the service to update the commission
    private fun addMenu() {
        Functions.showProgress(mContext!!)
        mDisposable = CompositeDisposable()
        try {
            mDisposable.add(
                App.getNetworkService().saveMenu(
                    name!!.text.toString(), price!!.text.toString().toDouble(),category!!.text.toString
                        (), this.trade.id
                ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({ menu ->
                    Functions.hideProgress()
                    dialog!!.dismiss()
                    Log.e("meal added", Gson().toJson(menu))
                    //refresh the view to show change
                    listener.menuAdded(menu)

                },
                { error ->
                    Functions.hideProgress()
                    dialog!!.dismiss()
                    Functions.toastLong(mContext!!, "Erreur lors de l'enregistrement. Veuillez reessayer")
                })
            )
        } catch (ex: Exception) {
            Functions.hideProgress()
            dialog!!.dismiss()
            Functions.toastLong(mContext!!, "Erreur lors de l'enregistrement. Veuillez reessayer")
        }
    }

    fun  checkFields(): Boolean {

        var verif = false;

        if (name!!.getText().toString().trim().equals("")) {
            name!!.setError(mContext!!.getString(R.string.fill_field));
            verif = true;
        }
        if (price!!.getText().toString().trim().equals("")) {
            price!!.setError(mContext!!.getString(R.string.fill_field));
            verif = true;
        }
        if (category!!.getText().toString().trim().equals("")) {
            category!!.setError(mContext!!.getString(R.string.fill_field));
            verif = true;
        }

        return verif;
    }

    companion object {

        fun Builder(ctx: Context, trade: Trade, listener: ProductsFragment.ProductMenuFragment.Listener): AddMenuDialog {
            return AddMenuDialog(ctx, trade, listener)
        }
    }
}
