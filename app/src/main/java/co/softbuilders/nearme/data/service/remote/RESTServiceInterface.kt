package co.softbuilders.nearme.data.service.remote

import co.softbuilders.nearme.data.models.Photo
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.data.models.TradeFull
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


/**
 * Created by Samuel on 7/1/19.
 *
 * Define your Remote REST service
 */

interface RESTServiceInterface {
    /**
     * You must define your web serices here @see @link(https://square.github.io/retrofit/)
     */

    //i have to made a personnalized service to save trade
    // in this service we should check traderphone Number existence and send code to user created
    //at the end of the process
/*
    //save a trade and hos owner with the appropriate service
    @Multipart
    @POST("trade/saveWithAvatar")
    fun saveTrade(@Part("data") trade: Trade,@Part avatar: MultipartBody.Part): Observable<Trade>*/


    //save a trade and hos owner with the appropriate service
    @Multipart
    @POST("trade/saveWithAvatar")
    fun saveTrade(
        @Part("name") name: RequestBody,
        @Part("tradeType") tradeType: RequestBody,
        @Part("tradePhoneNumber") tradePhoneNumber: RequestBody,
        @Part("password") password: RequestBody,
        @Part("traderFirstName") traderFirstName: RequestBody,
        @Part("traderLastName") traderLastName: RequestBody,
        @Part("traderPhoneNumber") traderPhoneNumber: RequestBody,
        @Part avatar: MultipartBody.Part): Observable<Trade>


//
//    //update trade infos
//    @PUT("trade/{tradeID}")
//    @FormUrlEncoded
//    fun updateTradeInfos(
//        @Path("tradeID") tradeID: String,
//        @Field("name") name: String,
//        @Field("tradeType") tradeType: String,
//        @Field("tradePhoneNumber") tradePhoneNumber: String,
//        @Field("password") password: String): Observable<Trade>
//
//
//    //update trade infos
//    @PUT("trade/{tradeID}")
//    @FormUrlEncoded
//    fun updateTraderInfos(
//        @Path("tradeID") tradeID: String,
//        @Field("traderFirstName") traderFirstName: String,
//        @Field("traderLastName") traderLastName: String): Observable<Trade>


    //get trade infos
    @GET("trade/{tradeID}")
    fun getTradeInfos(
        @Path("tradeID") tradeID: String): Observable<Trade>


    //get favorite meals poviding a tradeID
    ///favoriteMeal/fecthByTradeID?tradeID=5ced4a2c480c475666225b5d
    @GET("/favoriteMeal/fecthByTradeID")
    fun getFavoriteMealsByTradeID(
        @Query("tradeID") tradeID: String): Observable<MutableList<TradeFull.FavoriteMeal>>

    //get favorite menus poviding a tradeID
    ///menu/fecthByTradeID?tradeID=5ced4a2c480c475666225b5d
    @GET("/menu/fecthByTradeID")
    fun getMenusByTradeID(
        @Query("tradeID") tradeID: String): Observable<MutableList<TradeFull.Menu>>



    //code verification step
    @POST("trade/verifyOtp")
    @FormUrlEncoded
    fun verifyCodeOtp(@Field("traderPhoneNumber") traderPhoneNumber: String
                      ,@Field("userCode") userCode: String): Observable<Trade>

    //login newly created user
    //please be sure user have verify his phoneNumber before trying to login into the system (backend)
    @POST("trade/login")
    @FormUrlEncoded
    fun traderLogin(@Field("traderPhoneNumber") traderPhoneNumber: String
                      ,@Field("password") password: String): Observable<Trade>


    //update the trader owner informations
    @PUT("trade/{tradeID}")
    @FormUrlEncoded
    fun updateTraderOwner(@Path("tradeID") tradeID: String,
                          @Field("traderFirstName") traderFirstName: String,
                          @Field("traderLastName") traderLastName: String,
                          @Field("traderPhoneNumber") traderPhoneNumber: String
                        ): Observable<Trade>


    //update the trade informations
    @PUT("trade/{tradeID}")
    @FormUrlEncoded
    fun updateTrade(@Path("tradeID") tradeID: String,
                          @Field("name") name: String,
                          @Field("tradeType") tradeType: String,
                          @Field("tradePhoneNumber") tradePhoneNumber: String,
                          @Field("password") password: String
    ): Observable<Trade>


    //display uploded picture into the drawer the rounded way
    //call the appropriate service to download the image
    // http://localhost:8083/trade/avatar/5ce431669c189f6042f52aa7
    @GET("trade/avatar/{userID}")
    @FormUrlEncoded
    fun retriveAvatar(@Path("userID")  userID:String): Observable<Trade>

    //save a photo and add it as ressource for every model that needs it
    @Multipart
    @POST("photo/upload")
    fun uploadPhoto(@Part photo: MultipartBody.Part): Observable<Photo>


    //save a meal after uploading it picture trough the  uploadPhoto service
    @POST("favoriteMeal/save")
    @FormUrlEncoded
    fun saveFavoriteMeal(
        @Field("name") name: String,
        @Field("price") price: Double,
        @Field("tradeID") tradeID: String,
        @Field("photo") photoID: String
    ): Observable<TradeFull.FavoriteMeal>


    //code verification step
    @POST("menu/save")
    @FormUrlEncoded
    fun saveMenu(
        @Field("name") name: String,
        @Field("price") price: Double,
        @Field("category") category: String,
        @Field("tradeID") tradeID: String
    ): Observable<TradeFull.Menu>




}
