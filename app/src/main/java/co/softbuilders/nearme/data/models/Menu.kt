package co.softbuilders.nearme.data.models

data class Menu(  var id:String =""){

    var name =""
    var category  = ""
    var price = 0.0
    lateinit var trade : Trade
}