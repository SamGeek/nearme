package co.softbuilders.nearme.data.models

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData

class TradeViewModel(application: Application) : AndroidViewModel(application){

    val trade : MutableLiveData<Trade> by lazy {
        MutableLiveData<Trade>()
    }

    val favoriteMealLoaded : MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

}