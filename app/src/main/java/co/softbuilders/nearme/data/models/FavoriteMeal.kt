package co.softbuilders.nearme.data.models

data class FavoriteMeal(var id:String =""){

    var name =""
    var price = 0.0
    //lateinit var photo : Photo //check later why it is not working
    var photo=""
    lateinit var trade : Trade
}