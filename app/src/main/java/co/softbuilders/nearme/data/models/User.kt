package co.softbuilders.nearme.data.models

import com.google.gson.annotations.Expose

data class User( val name: String,val phoneNumber: String,val email: String,val firmName: String,val business: String,val password: String,val parent: String, val role: String){
    @Expose(serialize = false, deserialize = true)
    var id =""
    var account : Double= 0.0
    @Expose(serialize = false, deserialize = true)
    var createdAt : Long= 0
}