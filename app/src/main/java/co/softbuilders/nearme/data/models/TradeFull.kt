package co.softbuilders.nearme.data.models

import com.google.gson.annotations.SerializedName

class TradeFull() : Trade(){


    @SerializedName("favoriteMeals")
    var favoriteMeals : MutableList<FavoriteMeal>? = null
    @SerializedName("menus")
    var menus : MutableList<Menu>? = null

    class FavoriteMeal(var id:String =""){

        var name =""
        var price = 0.0
        lateinit var photo : Photo
       // var photo ="" //check later how to solve this
    }

    data class Menu(  var id:String =""){

        var name =""
        var category  = ""
        var price = 0.0
    }
}

