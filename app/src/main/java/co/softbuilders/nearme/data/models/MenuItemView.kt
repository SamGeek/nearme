package co.softbuilders.nearme.data.models

data class MenuItemView(val menuImgResID : Int, val menuName: String )