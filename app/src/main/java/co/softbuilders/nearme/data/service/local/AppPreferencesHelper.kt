package co.softbuilders.nearme.data.service.local

import android.content.Context
import android.content.SharedPreferences
import co.softbuilders.nearme.data.models.Trade
import co.softbuilders.nearme.data.models.User
import com.google.gson.Gson


/**
 * Created by Samuel on 23/08/17.
 */

class AppPreferencesHelper(context: Context, prefFileName: String) {


    private val mPrefs: SharedPreferences

    var userConnected: User?
        get() = if (mPrefs.contains(CONNECTED_USER_KEY)) {
            Gson().fromJson(mPrefs.getString(CONNECTED_USER_KEY, ""), User::class.java)
        } else null
        set(user) {
            var sharedPreferencesEditor = mPrefs.edit()
            sharedPreferencesEditor.putString(CONNECTED_USER_KEY, Gson().toJson(user))
            sharedPreferencesEditor.apply()
        }


    var tradeConnected: Trade?
        get() = if (mPrefs.contains(CONNECTED_TRADE_KEY)) {
            Gson().fromJson(mPrefs.getString(CONNECTED_TRADE_KEY, ""), Trade::class.java)
        } else null
        set(trade) {
            var sharedPreferencesEditor = mPrefs.edit()
            sharedPreferencesEditor.putString(CONNECTED_TRADE_KEY, Gson().toJson(trade))
            sharedPreferencesEditor.apply()
        }


    init {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)
    }

    companion object {
        private val CONNECTED_USER_KEY = "CONNECTED_USER";
        private val CONNECTED_TRADE_KEY = "CONNECTED_TRADE"
    }
}
