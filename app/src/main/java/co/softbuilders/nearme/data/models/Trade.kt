package co.softbuilders.nearme.data.models

import com.google.gson.annotations.SerializedName

open class Trade(){

    var id :String =""
    var name =""
    var tradeType =""
    var tradePhoneNumber =""
    var password =""
    var avatarUrl =""
    var avatarName =""
    var avatarFd =""
    var code =""
    var traderFirstName =""
    var traderLastName =""
    var traderPhoneNumber =""
}

