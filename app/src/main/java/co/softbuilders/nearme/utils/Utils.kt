package co.softbuilders.nearme.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions.*


object Utils {

    fun glide(context: Context, url: String, v: ImageView, placeholder: Int, round: Boolean) {
        var request = Glide.with(context).load(url)
            .apply(diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .apply(placeholderOf(placeholder))
        if (round) {
            request = request.apply(bitmapTransform(CircleCrop()))
        }
        request.into(v)
    }

    fun glide(context: Context, imageRes: Int, v: ImageView) {
        Glide.with(context).load(imageRes)
            .apply(diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .into(v)
    }


}
