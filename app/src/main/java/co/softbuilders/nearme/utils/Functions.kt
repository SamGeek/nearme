package co.softbuilders.nearme.utils

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import android.os.CountDownTimer
import android.text.format.DateFormat
import android.widget.EditText
import android.widget.Toast
import co.softbuilders.nearme.R
import co.softbuilders.nearme.app.App
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.NumberFormat
import java.util.*


/**
 * Created by softbuilders on 18/01/17.
 */

object Functions {

    internal var i = 0
    private val timer: CountDownTimer? = null
    private val CHARS = "1234567890"
    var load: ProgressDialog? = null

    fun spacer(text: StringBuilder): StringBuilder {
        return StringBuilder("")
    }

    fun toastLong(mContext: Context, message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show()
    }

    fun toastShort(mContext: Context, message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
    }

    fun isEmpty(edt: EditText): Boolean {
        return edt.text.toString().trim { it <= ' ' }.equals("", ignoreCase = true)
    }


    fun checkEmptyFields(ed: EditText, errMsg : String =App.context!!.getString(R.string.fill_field)): Boolean {

        var verif = true

        if (ed.getText().toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
            ed.setError(errMsg)
            verif = false
        }

        return verif
    }

    fun checkPhoneNumber(ed: EditText, errMsg : String =App.context!!.getString(R.string.fill_field)): Boolean {

        var verif = true

        if (ed.getText().toString().trim({ it <= ' ' }).equals("", ignoreCase = true)
            || ed.getText().toString().trim({ it <= ' ' }).length < 8) {
            ed.setError("Veuillez entrer un numéro d'au moins 8 chiffres")
            verif = false
        }

        return verif
    }



    fun isTradeConnected():Boolean{
        return App.appPreferences.tradeConnected!=null
    }

    fun convertTimeStampToDate(timestamp: Long, format : String): String {
        val cal = Calendar.getInstance(Locale.FRENCH)
        cal.timeInMillis = timestamp
        return DateFormat.format(format, cal).toString()
    }


    fun getFileFromBitmap(signatureBitmap: Bitmap): File? {
        //create a file to write bitmap data
        var f: File? = null
        try {
            f = File(App.context!!.getCacheDir(), "avatar")
            f!!.createNewFile()
            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            signatureBitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
            val bitmapdata = bos.toByteArray()

            //write the bytes in file
            val fos = FileOutputStream(f)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }


        return f
    }


    fun showProgress(mContext: Context) {
        load = ProgressDialog(mContext)
        if (null == load) {
            load = ProgressDialog(mContext)
        }
        load!!.setCancelable(false)
        load!!.show()
    }

    fun hideProgress() {
        if (null != load) load!!.dismiss()
        load = null
    }


    fun formatAmount(nbr: Double): String {
        return NumberFormat.getInstance(Locale.ENGLISH).format(nbr).replace(",".toRegex(), ".")
    }

}
