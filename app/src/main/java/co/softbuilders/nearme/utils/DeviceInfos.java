package co.softbuilders.nearme.utils;

import android.preference.PreferenceManager;
import android.provider.Settings;
import co.softbuilders.nearme.app.App;


/**
 * Created by Samuel on 24/02/2017.
 */

public class DeviceInfos {
    public static final String FALSE_STRING = "-$1989829999";
    private static  String IMEI = FALSE_STRING;

    /**
     *
     * @return string Phone default (first sim imei on multi sim devices)
     */
    public static String getIMEI() {
        return IMEI != FALSE_STRING ? IMEI : innerGetIMEI() ;
    }

    private static String innerGetIMEI(){

        String TAG = "phoneUUID";
        String android_id = Settings.Secure.getString(App.Companion.getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        PreferenceManager.getDefaultSharedPreferences(App.Companion.getContext()).edit().putString(TAG,android_id).apply();

        return android_id;
    }
}

