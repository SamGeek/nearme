package co.softbuilders.nearme.app


import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.multidex.MultiDexApplication
import co.softbuilders.nearme.BuildConfig
import co.softbuilders.nearme.R
import co.softbuilders.nearme.data.service.local.AppPreferencesHelper
import co.softbuilders.nearme.data.service.remote.RESTServiceInterface
import co.softbuilders.nearme.utils.ReadOnly
import com.facebook.stetho.Stetho
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

import javax.net.ssl.*
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit

class App : MultiDexApplication() {


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        // The following line triggers the initialization of ACRA
        //        ACRA.init(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate() {
        super.onCreate()

        loadCalligrapghyFontForApp()

        synchronized(this) {
            if (instance == null) instance = this
        }

        //Initialise Stheto to view Database content
        Stetho.initialize(
            Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                .build()
        )

    }

    private fun loadCalligrapghyFontForApp() {
        CalligraphyConfig.initDefault(
            CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SourceSansPro-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
    }

    companion object {
        private val REST_END_POINT = BuildConfig.REST_END_POINT
        private var retrofit: Retrofit? = null
        private var networkService: RESTServiceInterface? = null
        /**
         * Get Instance of application singleon
         *
         * @return App
         */
        @get:ReadOnly
        var instance: App? = null
            private set
        private var preferencesHelper: AppPreferencesHelper? = null


        /**
         * Get context of application
         *
         * @return Context
         */
        val context: Context?
            @ReadOnly
            get() = instance!!


        @ReadOnly
        fun getRetrofit(): Retrofit {
            synchronized(App::class.java) {
                if (retrofit == null) {

                    val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate>,
                            authType: String
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate?> {

                            return arrayOfNulls(0)
                        }
                    })

                    // Install the all-trusting trust manager
                    val sslContext: SSLContext
                    var sslSocketFactory: SSLSocketFactory? = null
                    try {
                        sslContext = SSLContext.getInstance("SSL")
                        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
                        // Create an ssl socket factory with our all-trusting manager
                        sslSocketFactory = sslContext.socketFactory
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                    retrofit = Retrofit.Builder().baseUrl(REST_END_POINT)
                        .client(
                            OkHttpClient.Builder()
                                .readTimeout(5, TimeUnit.MINUTES)
                                .connectTimeout(5, TimeUnit.MINUTES)
                                .sslSocketFactory(sslSocketFactory!!, trustAllCerts[0] as X509TrustManager)
                                /* .addInterceptor(new Interceptor() {
                                            @Override
                                            public Response intercept(Chain chain) throws IOException {
                                                Request request = chain.request();
                                                Request authenticatedRequest = request.newBuilder()
                                                        .header("Authorization", Credentials.basic("QSUSR28","F54A22SDQ6CDU54OO183XX7T3")).build();
                                                return chain.proceed(authenticatedRequest);
                                            }
                                        })*/
                                .hostnameVerifier { s, sslSession -> true }.build()

                        )
                        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
                }
                return retrofit!!
            }
        }


        val appPreferences: AppPreferencesHelper
            @ReadOnly
            get() {
                if (null == preferencesHelper) {
                    synchronized(App::class.java) {
                        preferencesHelper = AppPreferencesHelper(context!!, context!!.getString(R.string.app_name))
                    }
                }
                return preferencesHelper!!
            }


        @ReadOnly
        fun getNetworkService(): RESTServiceInterface {
            synchronized(App::class.java) {
                if (networkService == null) {
                    networkService = getRetrofit().create(RESTServiceInterface::class.java)
                }
                return networkService!!
            }
        }


        //TODO : Declare once
        fun restartApp() {
            val i = App.context!!.packageManager
                .getLaunchIntentForPackage(App.context!!.packageName)
            i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            instance!!.startActivity(i)
        }
    }


}
